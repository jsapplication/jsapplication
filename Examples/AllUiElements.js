function DbActivityTool(params={},createDom=true) {
    jsa.Tool.call(this,params,false);
    
    //parameters
    this.containerStyle = ['jsa-tool-container','tool-db-connection'];
    this.showTime = 500; //ms
    this.activeStyle = 'tool-db-connection-active';
    this.onClickCallback = function (e) {
        this.ShowInfo();
    };

    jsa.CopyParams(this,params);

    //internals
    this.showTimeout = null;
    this.isActive = false;
    this.infoBubble = null;

    if(createDom) {
        this.CreateDom();
    }

    return this;
}

DbActivityTool.prototype = Object.create(jsa.Tool.prototype);

DbActivityTool.prototype.NotifyActivity = function() {
    if(this.showTimeout) {
        window.clearTimeout(this.showTimeout);
    }
    this.SetStateActive();
    var self = this;
    this.showTimeout = window.setTimeout(function() {
        self.SetStateInactive();
    },this.showTime);
    return this;
};

DbActivityTool.prototype.ShowInfo = function() {
    let app = this.GetApp();
    if(app) {
        if(!this.infoBubble) {
            this.infoBubble = new jsa.Bubble({
                name: '',
                enabled: true,
                resizable: false,
                content: 'Connected to ....',
                minimizable: false, 
                autoHide: true,
                closeable: false,
                borderOffset: 30, //px
                penaltyN: 0,
                penaltyE: 2000,
                penaltyS: 0,
                penaltyW: 2000,
                style: ['jsa-bubble','db-connection-info']
            });
            app.AddChild(this.infoBubble);
        }
        this.infoBubble.PopupOnDomElement(this.GetDomElement());
    }
    return this;
};

DbActivityTool.prototype.SetStateActive = function() {
    if(!this.isActive) {
        this.GetContainingDom().classList.add(this.activeStyle);
        this.isActive = true;
    }
    return this;
};

DbActivityTool.prototype.SetStateInactive = function() {
    if(this.isActive) {
        this.GetContainingDom().classList.remove(this.activeStyle);
        this.isActive = false;
    }
    return this;
};

DbActivityTool.prototype.Dissolve = function() {
    if(this.infoBubble) {
        this.infoBubble.Dissolve();
    }
    jsa.Tool.prototype.Dissolve.call(this);
}



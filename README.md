![jsApplication Logo](Media/Logo/JsaLogoRed.svg)

# jsApplication: Desktop-like applications in JavaScript

jsApplication is a JavaScript framework to build desktop-like applications as single-page, client-only HTML pages. It provides classes for UI elements that are to be used as UI programming in object-oriented languages as Java or C#. jsApplication provides standard UI elements as View, Containers, Menu, Button, Table, MsgBox, Modal, Tabbar, Splash, TextCtrl, SelectCtrl, RadioCtrl, CheckboxCtrl. In addition, it includes unorthodox UI elements as Sticky, Bubble, Dash, and Tool. jsApplication includes CSS definitions helping to define a customizable, but consistent looking theme for an application. In order to use jsApplication absolutely no server-side script or calculation is necessary, i.e. no server at all is necessary. A pure .html file and a browser is sufficient.

* Website: https://jsapplication.gitlab.io/jsapplication/

## Use and learn jsApplication

In order to use jsApplication you have to include jsapplication.css and jsapplication.js as well as the its img folder included in html dir. Usually that is contained in the jsa directory, which can be obtained from the jsa library repository (see below)

* Library repository: https://gitlab.com/jsapplication/jsa/ 

In order to understand and learn jsApplication it is advices to take a look at the documentation and the examples contained in the main repository

* Main repository (including Documentation and Examples): https://gitlab.com/jsapplication/jsapplication/ 
* API-Documentation: https://jsapplication.gitlab.io/jsapplication/index.html?view=documentation

If that is to lengthy for you, try the following getting-started example.


## Getting-started example

Include _jsapplicaiton.css_ in your html file:

    <link href="css/jquery-ui.min.css" rel="stylesheet"/>
    <link href="jsa/jsapplication.css" rel="stylesheet"/>


Include _jsapplication.js_ in and contrib scripts in your html file:

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="jsa/jsapplication.js"></script>

Create an application with exactly one view:

    <body>
        <script>
            let myApp = new jsa.Application( {
                name:'My First Application',
            });
            let myView = new jsa.View({
            name:'Welcome',
            content:'Hello World!'
            });
            myApp.viewManager.AddChild(myView);
            myApp.viewManager.ActivateView(myView);
        </script>
    </body>
	
View the result in your browser:

![Hello World Example Screenshot](Media/HelloWorld.png)
 

## Projects using jsAppliation
* AUTOSAFE – A prototype of a tool deriving fault-trees from system models automatically: https://www.ils.uni-stuttgart.de/forschung/adaptive_plattform/autosafe/
* B Annighoefer, M. Brunner, J. Schoepf, B. Luettig, M. Merckling, and P. Mueller, "Holistic IMA Platform Configuration using Web-technologies and a Domain-specific Model Query Language," IEEE/AIAA Digital Avionics Systems Conference (DASC), Oct. 2020, https://doi.org/10.1109/DASC50938.2020.9256726

## License
jsApplication is Open-Source released under the [MIT license](LICENSE).

## Acknowledgements
jsApplication currently depends on the following third-party framework. Thanks for the great work. 

* jQuery is used as the backbone for jQueryUi: https://jquery.com/
* jQueryUi is used as for resizing resizable and draggable UI elements, e.g. Sticky and Bubble: https://jqueryui.com/
* The js application website uses showdown js to show this information: http://showdownjs.com/

Look into the Contrib folder for the specific version used for each of the third-party frameworks.

![Jsa Guy says goodbye](Media/Logo/JsaSmileyRed.svg)

